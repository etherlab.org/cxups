#!/usr/bin/env python3

import gi.repository.GLib
import dbus
import dbus.mainloop.glib
import os

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)


def signal_handler(interface, prop, *__args):
    """Signal hander for asynchronous dbus signals"""

    # We just want to know when org.freedesktop.UPower.Device interface
    # property Online is false. Reject everything else.
    if not (interface == 'org.freedesktop.UPower.Device'
            and isinstance(prop, dbus.Dictionary)
            and 'Online' in prop):
        return

    online = prop['Online']
    print(f'Online: {online}')

    if not online:
        print('Initiate system shutdown...')
        os.system("poweroff")


# Setup system bus to wait for signals
dbus.SystemBus().add_signal_receiver(
        signal_handler,
        signal_name='PropertiesChanged',
        dbus_interface='org.freedesktop.DBus.Properties')

print('Power monitor running.')
mainloop = gi.repository.GLib.MainLoop()
mainloop.run()
