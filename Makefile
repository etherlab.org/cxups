
ifneq ($(KERNELRELEASE),)
obj-m := cx51xx_ups.o

else
KDIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)

all modules:
	$(MAKE) -C $(KDIR) M=$(PWD) V=$(VERBOSE) modules

install modules_install:
	$(MAKE) -C $(KDIR) M=$(PWD) V=$(VERBOSE) modules_install

endif

clean:
	rm -rf *.o *.ko *.cmd *.mod.* modules.order
	rm -rf Module.symvers .tmp_versions

