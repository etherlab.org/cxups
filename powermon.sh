#!/bin/bash

path=/org/freedesktop/UPower/devices/line_power_cxups
interface=org.freedesktop.DBus.Properties
member=PropertiesChanged

# Wait for activity on the system bus
dbus-monitor --profile --system "path='${path}',interface='${interface}',member='${member}'" |
while read -r line; do

    # This dbus-send command queries dbus and returns a reply like:
    #   variant       boolean true
    # The set command populates the environment variables $1, $2 and $3
    # with these words. Most important is $3 which is either
    # "false" on power fail or "true" on power good
    set $(dbus-send --system --print-reply=literal --dest=org.freedesktop.UPower \
        ${path} ${interface}.Get string:org.freedesktop.UPower.Device string:Online)

    if test "$3" = "false"; then
        # Power failed. Here you can put your commands to be executed
        # e.g. systemctl halt
        echo "Power fail"
    else
        # Power returned
        # In the event that the system was not shut down
        echo "Power back on"
    fi
done
