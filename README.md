vim: spell spelllang=en syntax=markdown tw=78

# Driver for the 1sec UPS in the Beckhoff CX51xx Series of embedded IPC

## Kernel Module

### Compile the module and install it

```bash
make
sudo make install
```

### Setup module for automatic insertion

Create a file /etc/modules-load.d/cx51xx_ups.conf with the module name to
load.

```bash
sudo sh -c 'echo "cx51xx_ups" > /etc/modules-load.d/cx51xx_ups.conf
```

### Debugging

```bash
sudo sh -c "echo 'module cx51xx_ups +p' > /sys/kernel/debug/dynamic_debug/control"
```

# User Space Tools

## Install dependencies

```bash
sudo zypper install upower libupower-glib3 python3 python3-gobject2
```

## Shell Script

Write a shell script that listens on DBus. See /usr/local/bin/powermon.sh

## Python Script

Or even simpler, a python script: See powermon.py

### Systemd Integration of Python Script

Integrate this into systemd:

```bash
sudo cp powermon.py /usr/local/sbin/
sudo chmod +x /usr/local/sbin/powermon.py
sudo cp powermon.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable powermon.service
sudo systemctl start powermon.service
```
