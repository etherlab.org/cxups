/**********************************************************************
 * Power supply driver for Beckhoff CX5120 1sec UPS
 *
 * The CX5120 series of industrial PC's have a small integrated UPS
 * that is capable of sustaining power for a short while. This is
 * just enough so that services are capable of shutting down
 * gracefully in the event of a supply failure.
 *
 * This driver interfaces to the kernel's power supply subsystem
 * to report the online state of power. It is up to user space
 * utilities to deal with these signals.
 *
 * Copyright (C) 2015, Richard Hacker, IgH GmbH
 *               2016, Florian Pose <fp@igh.de>
 * License: GPL
 *
 **********************************************************************/

#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/module.h>
#include <linux/workqueue.h>
#include <asm/io.h>

static struct platform_device *cxups_pdev = NULL;
struct power_supply *cxups_psy = NULL;

/**********************************************************************
 * Test function:
 * Bit 22 of port 0x588: 0 = mains, 1 = battery
 **********************************************************************/
inline int online(void) {
    return !(inl(0x588) & 0x400000);
}

/**********************************************************************
 * DC power properties
 **********************************************************************/
static int cxups_get_prop(struct power_supply *psy,
        enum power_supply_property psp,
        union power_supply_propval *val)
{
    switch (psp) {
        case POWER_SUPPLY_PROP_ONLINE:
            val->intval = online();
            break;

        default:
            return -EINVAL;
    }

    return 0;
}

static enum power_supply_property cxups_props[] = {
    POWER_SUPPLY_PROP_ONLINE,
};

static struct power_supply_desc cx_mains_desc = {
    .name = "cxups",
    .type = POWER_SUPPLY_TYPE_MAINS,
    .properties = cxups_props,
    .num_properties = ARRAY_SIZE(cxups_props),
    .get_property = cxups_get_prop,
};

/**********************************************************************
 * Monitor
 **********************************************************************/
static void monitor_supply_work(struct work_struct *work);
DECLARE_DELAYED_WORK(workq, monitor_supply_work);

static void monitor_supply_work(struct work_struct *work)
{
    static int online_state = -1;
    int state = online();

    if (online_state != state) {
        online_state = state;

        dev_dbg(&cxups_pdev->dev, "MAINS=%i\n", state);

        power_supply_changed(cxups_psy);
    }

    schedule_delayed_work(&workq, HZ/10);
}

/**********************************************************************
 * Module initialization
 **********************************************************************/
static int __init ups_init(void)
{
    int rv;

    cxups_pdev = platform_device_register_simple("cxups", 0, NULL, 0);
    if (IS_ERR(cxups_pdev)) {
        rv = PTR_ERR(cxups_pdev);
        goto pdev_register_failed;
    }

    cxups_psy = power_supply_register(&cxups_pdev->dev,
            &cx_mains_desc, NULL);
    if (IS_ERR(cxups_psy)) {
        rv = PTR_ERR(cxups_psy);
        goto power_supply_register_failed;
    }

    schedule_delayed_work(&workq, HZ/10);

    return 0;

power_supply_register_failed:
    platform_device_unregister(cxups_pdev);
pdev_register_failed:
    return rv;
}

/**********************************************************************/
static void __exit ups_exit(void)
{
    cancel_delayed_work_sync(&workq);

    power_supply_unregister(cxups_psy);
    platform_device_unregister(cxups_pdev);
}

module_init(ups_init);
module_exit(ups_exit);

MODULE_DESCRIPTION("CX5120 1sec UPS driver");
MODULE_AUTHOR("Richard Hacker <ha@igh.de>");
MODULE_LICENSE("GPL");
